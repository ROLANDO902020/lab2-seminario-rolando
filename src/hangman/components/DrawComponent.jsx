import React from "react";
import { useCounter } from "../hooks/useCounter";

export const DrawComponent = () => {
    const [counter, incrementCounter, decrementCounter] = useCounter (0, {max:6, min:0})
    return (
        <div>
            <img src={`/assets/${counter}.jpg`} alt="test" />
            <h1>{counter}</h1>
            <button type="button" class="btn btn-outline-danger" onClick={() => decrementCounter()}>Increment</button>
            <button type="button" class="btn btn-outline-warning" onClick={() => incrementCounter()}>Increment</button>
        </div>
    );
};
export default DrawComponent;